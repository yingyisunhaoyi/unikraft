#!/bin/bash

# Copyright 2023 Hangzhou Yingyi Technology Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# 本脚本用于检测PR标题是否符合规范，需遵循格式
# "#TEN-A B:" 或 "#TEN-A B/C:" 或 "#TEN-A B/C[D]:" 或 "#TEN-A B1/C, B2/D:"
# A为TenonOS项目编号（纯数字）
# B为"lib"、"app"、"plat"、"arch"、"build"、"ci"之一
# C为细化模块名（任意字母、数字、'-'的组合）
# D为备注（任意字母、数字、'-'的组合）
# C、D参数可选
# ex1: #TEN-666 build: change build rules
# ex2: #TEN-666 app/helloworld: add helloworld function
# ex3: #TEN-666 lib/posix-time: fix a bug
# ex4: #TEN-666 plat/raspi[bugFix]: Improve performance
# ex5: #TEN-666 plat/raspi, lib/ukvmem: Adapt changes from uk v0.15
story_id='^#TEN-[1-9][0-9]*'
module='(lib|app|plat|arch|build|ci)'
sub_module='(/[-0-9a-zA-Z]+(\[[-0-9a-zA-Z]+])?)?'

title_exp="${story_id} ${module}${sub_module}(, ${module}${sub_module})*: "

if [[ ! $1 =~ $title_exp ]] ;then
        echo -e "check PR title fail!! PR标题格式错误\n"
	echo -e "$1\n"
	echo "示例：\"#TEN-A B:\" 或 \"#TEN-A B/C:\" 或 \
\"#TEN-A B/C[D]:\" 或 \"#TEN-A B1/C, B2/D:\""
        echo "需匹配正则表达式: $title_exp"
        exit 1
fi

echo "check PR title success!!"
