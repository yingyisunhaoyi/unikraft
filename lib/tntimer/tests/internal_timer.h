/* Copyright 2024 Hangzhou Yingyi Technology Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __TN_INTERNAL_TIMER_H__
#define __TN_INTERNAL_TIMER_H__

void tn_timer_list_switch(void);

struct uk_list_head *tn_timer_get_overflow_list(void);

/*
 * tn_timer_get_list - Retrieves the global timer list.
 *
 * Returns a pointer to the head of the timer list.
 */
struct uk_list_head *tn_timer_get_list(void);
#endif /* __TN_INTERNAL_TIMER_H__ */
