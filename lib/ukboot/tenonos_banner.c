/* Copyright 2024 Hangzhou Yingyi Technology Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ssANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <uk/config.h>

void print_banner(FILE *out)
{
	fprintf(out, "\n __          __  _                            _          _______                      ____   _____\n");
	fprintf(out, " \\ \\        / / | |                          | |        |__   __|                    / __ \\ / ____|\n");
	fprintf(out, "  \\ \\  /\\  / /__| | ___ ___  _ __ ___   ___  | |_ ___      | | ___ _ __   ___  _ __ | |  | | (___  \n");
	fprintf(out, "   \\ \\/  \\/ / _ \\ |/ __/ _ \\| '_ ` _ \\ / _ \\ | __/ _ \\     | |/ _ \\ '_ \\ / _ \\| '_ \\| |  | |\\___ \\ \n");
	fprintf(out, "    \\  /\\  /  __/ | (_| (_) | | | | | |  __/ | || (_) |    | |  __/ | | | (_) | | | | |__| |____) |\n");
	fprintf(out, "     \\/  \\/ \\___|_|\\___\\___/|_| |_| |_|\\___|  \\__\\___/     |_|\\___|_| |_|\\___/|_| |_|\\____/|_____/ \n\n\n\n");
#ifdef CONFIG_LIBTNBOOT_BANNER_WELCOME_IMG_AND_TXT
	fprintf(out, "                  ::::: :\n");
	fprintf(out, "              ::::::::  :::\n");
	fprintf(out, "             ::::::::: :::::\n");
	fprintf(out, "            .:::::::: ::::::::\n");
	fprintf(out, "            ::::::::  :::::::::\n");
	fprintf(out, "           ::::.      ::::::::.\n");
	fprintf(out, "            ::::::::::  ::::::                    G:\n");
	fprintf(out, "             ::::::::::: ::::                  GGGGGLG;\n");
	fprintf(out, "               ::::::::::  :.               DGGGGGGGGGGGL\n");
	fprintf(out, "                 ::::::::::              GDGGGGGGGGGGGGGG.E\n");
	fprintf(out, "                                      GDDDDGGGGGGGGGGG:KWWW\n");
	fprintf(out, "                                   DDDDDDDDDGGGGGGGGGG: WWW\n");
	fprintf(out, "                                  W  DDDDDDDDGGGGGGGGGGGG L       GGGG\n");
	fprintf(out, "                 GGG.              WWW  DDDDDDDGGGGGGGGGGGGG   LLLLLLLLLL\n");
	fprintf(out, "              GDDDDGGGG.           KWWWWW  DDDDDGGGGGGGGGGGGGGGLLLLLLLLLLLLG\n");
	fprintf(out, "           DDDDDDDDGGGGGGGj        DWK  DDDDDDDDDGGGGGGGGGGGGGGGLLLLLLLLLLLLLL\n");
	fprintf(out, "           DEDDDDDDDDGGGGGGGGG       DDDDDDDDDDDDGGGGGGGGGGGGGGGGLLLLLLLLLL  ED\n");
	fprintf(out, "         LGL  LDDDDDDDGGGGGGGGGG  EEEDDDDDDDDDDDDDDGGGGGGGGGGGGGGGGLLLLG  DDDDD\n");
	fprintf(out, "         GLLLGG. :DDDDDGGGGGG  EEEEEEEDDDDDDDDDDD  GGL :GGGGGGGGGGGLG  DDDDDDDD\n");
	fprintf(out, "         LLLLLLGGLt  GDDGG  KEEEEEEEEEDDDDDDDD  DDDDDGGGG  GGGGGGG  DDDDDDDDDDD\n");
	fprintf(out, "         LLLLLGGGGGGGG   DKKKEEEEEEEEEEEDDE  DDDDDDDDGGGGGGG  G  DDDDDDDDDDDDDD\n");
	fprintf(out, "         LLLLGGGGGGGGGGG   KKKEEEEEEEEEE  t K  DDDDDDDGGGGGG   DDDDDDDDDDDDGGGG;\n");
	fprintf(out, "          LGLGGGGGGGGGGG GGD  KEEEEEE  iEEE;KKKK, LDDDDDG. EEE DDDDDDGGGGGGGGt\n");
	fprintf(out, "             GGGGGGGGGGG GGGGGD  E  LKKKKKKLWWKKKEEi t. KEEEEE DGGGGGGGGGGt\n");
	fprintf(out, "               GGGGGGGGG GGGDDDDD jKKKKKKKKEEWWKKKEEEjDEEEEEEE GGGGGGGGj                  :j\n");
	fprintf(out, "               GGGGGGGGG GDDDDDDD .KWWWWWWWWGWWWKKKEEE,EEEEEEEfiGGGGj            f LLLLLLLLLL\n");
	fprintf(out, "               GGGGGGGGG GDDDDDDD. WWWWWWWWW;WWWWKKKEE EEEEEDED:Gf               LL LLLLLLLLLL\n");
	fprintf(out, "           :WW GGGGGGGGG fGDDDDDD. WWWWWWWK  DKWWWKKKE DDDDDDDD                 LLLL LLLLLLLLLL.\n");
	fprintf(out, "         .WWWW GGGGGGGGGGDG iDDDDt WWWWK iWWKKK jKWKKK DDDDDDDD                LLLLLL LLLLLLLLLLL\n");
	fprintf(out, "         GG  W GGGGGGGDGDDDDDD ,DD WE ,WWWWKKKEEEE ,KK DDDDDDDD K;            LLLLLLLG  ;       f\n");
	fprintf(out, "         GGGGG GGGGGGGDDDDDDDDDDD  jWWWWWKKKKEEEEDDDD  DDDDDDDD WWWW:         iLLLLLLL :LLLLLLLL\n");
	fprintf(out, "         GGGGGGGGGGGDDDDDDDDDDDDDDDD  WKKKKKEEEEDDD  D DDDDDDDD WWWWWWW         LLLLLG LLLLLLLL\n");
	fprintf(out, "         GGGGGGGGGGDDDDDDDDDDDDDDDDEEEE  KEEEEED  DDDD DDDDDDDD WWWW  DG         LLLL LLLLLLLL\n");
	fprintf(out, "         GGGGGGGGGGGDDDDDDDDDDDDDDEEEEEEEE  D  DDDDDDD DDDDDDGG K  GGGGG          LL LLLLLLLL.\n");
	fprintf(out, "           GGGGGGGDDDDDD  GDDDDDDEEEEEEEEEEE DDDDDDDDG GGGGGGGGjLGGGGGGG            jf\n");
	fprintf(out, "              DGDDDDDDDD WWW jDDEEEEEEEEEEEE DGDDDGGGG GGGGGGGGGGGGGGGGG\n");
	fprintf(out, "                 GDDDDDD WWWW;  ,EEEEEEEEEEE GGGGGGGGG GGGGGGGGGGGGGGGGG\n");
	fprintf(out, "                    fDDD KW         EEEEEEKK GGGGGGGG  GGGGGGGGGGGGGGGGG\n");
	fprintf(out, "                       ;           GD  EEKKK GGGGG  DGGGGGGGGGGGGGGGGGGG\n");
	fprintf(out, "                                   DDDDD: KK GG  DGGGGGGGGGGGGGGGGGGGGGG\n");
	fprintf(out, "                                   GDDDDDDE   jGGGGGGGGGGGGGGGG.GGGGGGGG\n");
	fprintf(out, "                                   GDDDDDDEE GGGGGGGGGGGGGGG  W GGGGGGGG\n");
	fprintf(out, "                                   DDDDDDEEE GGGGGGGGGGGL. KWWW GLLGGLf\n");
	fprintf(out, "             ,LLLLLLLLLL           DDDDDDEEE LGGGGGGGGG   iWWWW GLLL\n");
	fprintf(out, "            LL LLLLLLLLLL          DDDDDDEEE.GGLLLGGGGL       . L\n");
	fprintf(out, "           GLLL LLLLLLLLLLL        DDDDDEEEE:GLLLLLLLLL\n");
	fprintf(out, "           LLLLG ,LLLLLLLLLL       DDDDDEEEE,GLLLLLLLLL\n");
	fprintf(out, "          LLLLLLLL LLLLLLt:        DDDDDEEEE;GLLLLLLLLL\n");
	fprintf(out, "          LLLLLLLL. fLLLLLLLL       .DDEEEEEtLLLLLLG;\n");
	fprintf(out, "           LLLLLLL LLLLLLLLL            EEEEfLLLL;\n");
	fprintf(out, "            LLLLL tLLLLLLLL.               KGLi\n");
	fprintf(out, "             LLLf LLLLLLLLL\n");
	fprintf(out, "              GL LLLLLLLLL\n\n\n");
#endif
}

